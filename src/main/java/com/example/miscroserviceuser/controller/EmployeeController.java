package com.example.miscroserviceuser.controller;

import com.example.miscroserviceuser.dto.RequestEmployeeDto;
import com.example.miscroserviceuser.dto.ResponseEmployeeDto;
import com.example.miscroserviceuser.exception.NotFoundException;
import com.example.miscroserviceuser.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
@CrossOrigin(value = "http://localhost:8083",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PATCH})
public class EmployeeController {


    private final EmployeeService _employeeService;


    public EmployeeController(EmployeeService employeeService) {
        _employeeService = employeeService;
    }


    @PostMapping("")
    public ResponseEntity<ResponseEmployeeDto> createEmployee(@RequestBody RequestEmployeeDto requestEmployeeDto) {
        return new ResponseEntity<>(_employeeService.createEmployee(requestEmployeeDto), HttpStatus.CREATED);
    }


    @GetMapping("")
    public ResponseEntity<List<ResponseEmployeeDto>> getAllEmployee() {
        return new ResponseEntity<>(_employeeService.findAllEmployee(), HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseEmployeeDto> getEmployeeById(@PathVariable int id) {
        return ResponseEntity.ok(_employeeService.getEmployeeById(id));
    }


    @DeleteMapping("{id}")
    public ResponseEntity<ResponseEmployeeDto> deleteEmployee(@PathVariable int id) {

        _employeeService.deleteEmployeeById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/search/{search}")
    public ResponseEntity<List<ResponseEmployeeDto>> getEmployee(@PathVariable String search) throws NotFoundException {


//        try {
////            return (ResponseEntity<ResponseEmployeeDto>) ResponseEntity.ok(_employeeService.getEmployee(search + "%"));
//            return  ResponseEntity<>(_employeeService.getEmployee(search+"%"));
//        } catch (NotFoundException e) {
//            return ResponseEntity.notFound().build();
//        }

        return new ResponseEntity<>(_employeeService.getEmployee(search),HttpStatus.OK);
    }

    @GetMapping("/login/{search}")
    public ResponseEntity<ResponseEmployeeDto> getEmployeeLogin(@PathVariable String search){

        try {
            return ResponseEntity.ok(_employeeService.getEmployeeLogin(search));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }


    }
}
