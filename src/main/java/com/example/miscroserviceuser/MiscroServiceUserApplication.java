package com.example.miscroserviceuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiscroServiceUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiscroServiceUserApplication.class, args);
	}

}
