package com.example.miscroserviceuser.repository;

import com.example.miscroserviceuser.entity.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Integer> {

    @Query("SELECT c FROM Employee c where c.firstName like %?1% or c.lastName like %?1%")
    public List<Employee> searchEmployee(@Param("search")String search);

    @Query("SELECT c FROM Employee c where c.lastName like :name and c.firstName like :first")
    public Employee searchEmployeeByLastNameAndFirstName(@Param("name")String name, @Param("first")String first);


}
