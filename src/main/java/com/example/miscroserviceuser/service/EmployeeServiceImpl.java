package com.example.miscroserviceuser.service;

import com.example.miscroserviceuser.dto.RequestEmployeeDto;
import com.example.miscroserviceuser.dto.ResponseEmployeeDto;
import com.example.miscroserviceuser.entity.Employee;
import com.example.miscroserviceuser.exception.NotFoundException;
import com.example.miscroserviceuser.repository.EmployeeRepository;

import org.mindrot.jbcrypt.BCrypt;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository _repository;


    private ModelMapper _modelMapper;


    public EmployeeServiceImpl( ModelMapper _modelMapper){
        this._modelMapper=_modelMapper;  }



    @Override
    public List<ResponseEmployeeDto> findAllEmployee() {
        List<Employee> employees = (List<Employee>) _repository.findAll();

        List<ResponseEmployeeDto> dtoList = new ArrayList<>();
        for (Employee e : employees) {

            ResponseEmployeeDto dto = _modelMapper.map(e,ResponseEmployeeDto.class);
            dtoList.add(dto);

        }

        return dtoList;


    }

    @Override
    public ResponseEmployeeDto getEmployeeById(int id) {

        return _modelMapper.map(findEmployee(id), ResponseEmployeeDto.class);
    }

    @Override
    public ResponseEmployeeDto createEmployee(RequestEmployeeDto employeeDto) {


        Employee employee = _modelMapper.map(employeeDto, Employee.class);
        String hashPass = BCrypt.hashpw(employeeDto.getPassword(),BCrypt.gensalt());
        employee.setPassword(hashPass);
        employee.setAuthorities(2);
        employee= _repository.save(employee);
        ResponseEmployeeDto responseEmployeeDto = _modelMapper.map(employee,ResponseEmployeeDto.class);

        return responseEmployeeDto;

    }


    @Override
    public List<ResponseEmployeeDto> getEmployee(String search) throws NotFoundException {
        List<Employee> employee = _repository.searchEmployee(search);

        List<ResponseEmployeeDto> dtoList = new ArrayList<>();
        for (Employee e : employee) {

            ResponseEmployeeDto dto = _modelMapper.map(e,ResponseEmployeeDto.class);
            dtoList.add(dto);

        }

        return dtoList;


//        if( employee == null){
//            throw new NotFoundException();
//        }
//        return Collections.singletonList(_modelMapper.map(employee, ResponseEmployeeDto.class));
    }




    @Override
    public ResponseEmployeeDto getEmployeeLogin(String search) throws NotFoundException {
        String[] words = search.split("\\.");

        Employee employee=_repository.searchEmployeeByLastNameAndFirstName(words[0], words[1]);
        if (employee == null){
            throw new NotFoundException();
        }

        return _modelMapper.map(employee,ResponseEmployeeDto.class);
    }

    @Override
    public ResponseEmployeeDto deleteEmployeeById(int id) {

        Employee employee = findEmployee(id);
        _repository.deleteById(id);

        return null;
    }



    Employee findEmployee(int id){
        Employee employee = _repository.findById(id).get();
        return employee;
    }




}
