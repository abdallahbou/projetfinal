package com.example.miscroserviceuser.service;

import com.example.miscroserviceuser.dto.RequestEmployeeDto;
import com.example.miscroserviceuser.dto.ResponseEmployeeDto;
import com.example.miscroserviceuser.exception.NotFoundException;

import java.util.List;

public interface EmployeeService {

    List<ResponseEmployeeDto> findAllEmployee();

    ResponseEmployeeDto getEmployeeById(int id);

    ResponseEmployeeDto createEmployee(RequestEmployeeDto employeeDto);

    List<ResponseEmployeeDto> getEmployee(String search) throws NotFoundException;

    ResponseEmployeeDto getEmployeeLogin(String search ) throws NotFoundException;

    ResponseEmployeeDto deleteEmployeeById(int id);


}
