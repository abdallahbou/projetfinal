package com.example.miscroserviceuser.service;

import com.example.miscroserviceuser.dto.RequestEmployeeDto;
import com.example.miscroserviceuser.dto.ResponseEmployeeDto;
import com.example.miscroserviceuser.entity.Employee;
import com.example.miscroserviceuser.exception.NotFoundException;
import com.example.miscroserviceuser.repository.EmployeeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

import static org.assertj.core.api.InstanceOfAssertFactories.LIST;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;


@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTest {


    @Mock
    EmployeeRepository employeeRepository;

    @Spy
    ModelMapper modelMapper;

    @InjectMocks
    private EmployeeServiceImpl employeeService;
    private Employee employee;

    private RequestEmployeeDto dto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
//        employee = new Employee();


    }

    @Test
    void testCreateEmployee() throws Exception {

        employee = new Employee();
        employee.setFirstName("toto");
        employee.setLastName("tata");
        dto =new RequestEmployeeDto();
        dto.setFirstName("toto");
        dto.setLastName("tata");


        given(employeeRepository.save(any())).willReturn(employee);
        // utilisé any() mais de mockito quand on l'instencie

        ResponseEmployeeDto saveE = employeeService.createEmployee(dto);

        assertThat(saveE).isNotNull();
    }

//
    @Test
    void testFindAllEmployee() throws Exception {
        employee = Employee.builder().firstName("toto").lastName("tata").build();
        Employee employee1 = Employee.builder().firstName("pika").lastName("chu").build();

        given(employeeRepository.findAll()).willReturn(List.of(employee, employee1));
        List<ResponseEmployeeDto> employees = employeeService.findAllEmployee();
        assertThat(employees).isNotNull();
    }

    @Test
    void testGetEmployeeById() throws Exception {
        employee = new Employee();
        employee.setFirstName("toto");
        employee.setLastName("tata");;


        given(employeeRepository.findById(0)).willReturn(Optional.ofNullable(employee));

        ResponseEmployeeDto saveEmployee = employeeService.getEmployeeById(employee.getId());

        assertThat(saveEmployee).isNotNull();
    }

    @Test
    void testDeleteEmployeeById() throws Exception{
        employee = new Employee();
        employee.setFirstName("toto");
        employee.setLastName("tata");

        given(employeeRepository.findById(0)).willReturn(Optional.ofNullable(employee));

      ResponseEmployeeDto employeeDto = employeeService.deleteEmployeeById(employee.getId());
        assertThat(employeeDto).isNull();


    }

    @Test
    void testGetEmployeeLogin() throws Exception {
        employee = new Employee();
        employee.setFirstName("toto");
        employee.setLastName("tata");
        String search = "toto.tata";
        String[] words = search.split("\\.");
        given(employeeRepository.searchEmployeeByLastNameAndFirstName(words[0], words[1])).willReturn(employee);
        ResponseEmployeeDto save = employeeService.getEmployeeLogin(search);

        assertThat(save).isNotNull();

    }

    @Test
    void testFindEmployee(){
        employee = new Employee();
        employee.setId(0);
        employee.setFirstName("toto");
        employee.setLastName("tata");

        given(employeeRepository.findById(0)).willReturn(Optional.ofNullable(employee));
        Employee employee1 = employeeService.findEmployee(0);
        assertThat(employee1).isNotNull();
    }

    @Test
    void testGetEmployee() throws NotFoundException {
        employee = new Employee();
        employee.setFirstName("toto");
        employee.setLastName("tata");
        Employee employee1 = new Employee();
        employee1.setFirstName("pika");
        employee1.setLastName("chu");
        Employee employee2 = new Employee();
        employee2.setFirstName("pika");
        employee2.setLastName("tortue");


        given(employeeRepository.searchEmployee("t")).willReturn(List.of(employee,  employee2) );
        List<ResponseEmployeeDto> responseEmployeeDtos = (List<ResponseEmployeeDto>) employeeService.getEmployee("t");
assertThat(responseEmployeeDtos).isNotNull();
    }

}
